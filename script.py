from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time


matricula = " "
senha = " "
driver = webdriver.Firefox()
driver.get("https://pergamum.ufsc.br/pergamum/biblioteca_s/php/login_usu.php?flag=index.php")
elem = driver.find_element_by_id("id_login")
elem.send_keys(matricula)
elem = driver.find_element_by_id("id_senhaLogin")
elem.send_keys(senha)
elem.send_keys(Keys.RETURN)


#dentro da página de renovação dos livros
time.sleep(2)
#daria pra pegar o número de elementos (livros) pra renovar. Fazer testes nos mesmos também.
i = 1
while i < 10:
	elem = driver.find_element_by_id("botao_renovar%d" % i)
	elem.click()
	time.sleep(2)
	elem = driver.find_element_by_id("btn_gravar4")
	elem.click()
	time.sleep(2)
	i += 1

print("%d livros renovados" % i)